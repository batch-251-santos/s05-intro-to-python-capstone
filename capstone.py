# s05 Capstone

from abc import ABC, abstractclassmethod

class Person (ABC):

	@abstractclassmethod
	def getFullName(self):
		...

	def addRequest(self):
		...

	def checkRequest(self):
		...

	def addUser(self):
		...


class Employee (Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self.firstName = firstName
		self.lastName = lastName
		self.email = email
		self.department = department

	#getters	
	def get_firstName(self):
		print(f"First name is: {self.firstName}")

	def get_lastName(self):
		print(f"Last name is: {self.lastName}")

	def get_email(self):
		print(f"Email is: {self.email}")

	def get_department(self):
		print(f"Department is: {self.department}")

	def getFullName(self):
		return (self.firstName + " " + self.lastName)

	#setters	
	def set_firstName(self, firstName):
		self.firstName = firstName

	def set_lastName(self, lastName):
		self.lastName = lastName

	def set_email(self, email):
		self.email = email

	def set_department(self, department):
		self.department = department 	

	def checkRequest(self):
		print(f"Checking database for request...")

	def addUser(self):
		print(f"User has been added.")

	def addRequest(self):
		return ("Request has been added.")

	def login(self):
		return (self.email + " has logged in.")

	def logout(self):
		return (self.email + " has logged out.")


class TeamLead (Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self.firstName = firstName
		self.lastName = lastName
		self.email = email
		self.department = department
		self.indiv_emp = []

	#getters	
	def get_firstName(self):
		print(f"First name is: {self.firstName}")

	def get_lastName(self):
		print(f"Last name is: {self.lastName}")

	def get_email(self):
		print(f"Email is: {self.email}")

	def get_department(self):
		print(f"Department is: {self.department}")

	def getFullName(self):
		return (self.firstName + " " + self.lastName)

	#setters	
	def set_firstName(self, firstName):
		self.firstName = firstName

	def set_lastName(self, lastName):
		self.lastName = lastName

	def set_email(self, email):
		self.email = email

	def set_department(self, department):
		self.department = department

	def checkRequest(self):
		print(f"Checking database for request...")

	def addUser(self):
		print(f"User has been added.")

	def login(self):
		return (self.email + " has logged in.")

	def logout(self):
		return (self.email + " has logged out.")

	def addMember(self, employee):
		self.indiv_emp.append(employee)
		return self.indiv_emp

	def get_members(self):
		return self.indiv_emp


class Admin (Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self.firstName = firstName
		self.lastName = lastName
		self.email = email
		self.department = department

	#getters	
	def get_firstName(self):
		print(f"First name is: {self.firstName}")

	def get_lastName(self):
		print(f"Last name is: {self.lastName}")

	def get_email(self):
		print(f"Email is: {self.email}")

	def get_department(self):
		print(f"Department is: {self.department}")

	def getFullName(self):
		return (self.firstName + " " + self.lastName)

	#setters	
	def set_firstName(self, firstName):
		self.firstName = firstName

	def set_lastName(self, lastName):
		self.lastName = lastName

	def set_email(self, email):
		self.email = email

	def set_department(self, department):
		self.department = department 	

	def checkRequest(self):
		print(f"Checking database for request...")

	def addUser(self):
		return ("User has been added.")

	def login(self):
		return (self.email + " has logged in.")

	def logout(self):
		return (self.email + " has logged out.")
		

class Request ():
	def __init__(self, name, requester, dateRequested):
		self.name = name
		self.requester = requester
		self.dateRequested = dateRequested
		self.status = "opened"

	def set_status(self, status):
		self.status = status

	def updateRequest(self):
		print(f"{self.name}'s current status is {self.status}'.")

	def closeRequest(self):
		self.status = "closed"
		return (self.name + " has been " + self.status + ".")

	def cancelRequest(self):
		self.status = "canceled"
		return (self.name + " has been " + self.status + ".")



employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Marketing")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Marketing")

admin1  = Admin("Monika", "Justin",  "jmonika@mail.com", "Marketing")

teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")

req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

# employee1.getFullName()
assert employee1.getFullName() == "John Doe", "Full name should be John Doe."
assert admin1.getFullName() == "Monika Justin", "Full name should  be Monika Justin."
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter."

assert employee2.login() == "sjane@mail.com has logged in."
assert employee2.addRequest() == "Request has been added."
assert employee2.logout() == "sjane@mail.com has logged out."


teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added."

req2.set_status("closed")
print(req2.closeRequest())